<?php
return [
    'adminEmail' => 'admin@example.com',
	'urlKgd' => 'http://kgd.gov.kz/ru/app/culs-taxarrear-search-web',
	'urlCapcha' => 'https://api.anti-captcha.com/createTask',
	'urlCapchaResult' => 'https://api.anti-captcha.com/getTaskResult',
];
