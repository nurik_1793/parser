<style>
.m-t {margin-top: 20px;}
.m-t-10 {margin-top: 10px;}
.p-0{padding: 0px !important;}
.p-5{padding: 5px !important;}
.p-15{padding: 15px !important;}
</style>

<div class="col-lg-4">
	<input type="text" class="form-control" id="iinBin" maxlength="12">
	<span class="error_iinBin m-t-10" style="display: none;"></span>
</div>

<button class="btn btn-primary" onclick="startFunction();" >Получить результат</button>


<div id="result" style="display: none;">
	<div class="col-lg-12 m-t">
		<div class="panel panel-default p-15">
			<div class="col-lg-12 p-5">
				<h3>По состоянию на <span id="sendTime"><?= date('d-m-Y');?></span></h3>
			</div>
			<table id="table0" class="table borderless m-t">
				<tbody><tr>
					<td id="result.table.1.row.1">Наименование налогоплательщика</td>
					<td class="font-bold" id="table1_name"></td>
				</tr>
				<tr>
					<td id="result.table.1.row.2">ИИН/БИН налогоплательщика</td>
					<td class="font-bold" id="table1_iinBin"></td>
				</tr>
				<tr>
					<td id="result.table.1.row.3">Всего задолженности (тенге)</td>
					<td class="font-bold" id="table1_totalArrear"></td>
				</tr>
			</tbody></table>
		</div>

		<div class="panel panel-default m-t p-15">
			<table id="table1" class="table borderless m-t">
				<tbody><tr>
					<td id="result.table.2.row.1">Итого задолженности в бюджет</td>
					<td class="font-bold tdWidth" id="table2_totalTaxArrear"></td>
				</tr>
				<tr>
					<td id="result.table.2.row.2">Задолженность по обязательным пенсионным взносам, обязательным профессиональным пенсионным взносам</td>
					<td class="font-bold tdWidth" id="table2_pensionContributionArrear"></td>
				</tr>
				<tr>
					<td id="result.table.2.row.med">Задолженность по отчислениям и (или) взносам на обязательное социальное медицинское страхование</td>
					<td class="font-bold tdWidth" id="table2_socialHealthInsuranceArrear"></td>
				</tr>
				<tr>
					<td id="result.table.2.row.3">Задолженность по социальным отчислениям</td>
					<td class="font-bold tdWidth" id="table2_socialContributionArrear"></td>
				</tr>
			</tbody></table>
		</div>

	</div>
</div>

<script>
$(document).ready(function () {
	initCaptcha();
});

// Валидация на правильность ввода ИИН/БИН
function validateUinCheckSumValue(uin) {
    for (var t = 1; t <= 3; t += 2) {
        var k = 0;
        for (var i = 0; i < 11; i++) {
            var w = (i+t)%11; if (w == 0) w = 11;
            k = (k+uin.substring(i, i+1) * w)%11;
        }
        if (k == 10) continue;
        return k == uin.substring(11,12);
    }
    return false;
}

// captcha
var captchaImg = null;
var captchaUid = null;
var iinBin = null;

// Создаем 1-ю капчу
function initCaptcha() {
	captchaUid = generateUUID();
	captchaImg = 'http://kgd.gov.kz/apps/services/CaptchaWeb/generate?uid=' + captchaUid + '&t=' + captchaUid;
	// console.log(captchaImg);
}

// Генарация капчи
function generateUUID() {
	var d = new Date().getTime();
	return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
        var r = (d + Math.random() * 16) % 16 | 0;
        d = Math.floor(d / 16);
        return (c == 'x' ? r : (r & 0x3 | 0x8)).toString(16);
    });
}

// Обновляем капчу
function reloadCaptcha() {
	var t = generateUUID();
	captchaImg = 'http://kgd.gov.kz/apps/services/CaptchaWeb/generate?uid=' + captchaUid + '&t=' + t;
	// console.log(captchaImg);
}

// Парсим капчу
function ParseCapcha(iinBin){	
	$.ajax({
		type : "POST",
		url: '/query/parse-capcha',			
		data : {captchaImg: captchaImg},
		success: function(data){
			var obj = jQuery.parseJSON(data);
			if(obj.errorId == 0){
				var taskId = obj.taskId;	
				ResultParseCapcha(taskId, iinBin);
			}
		}
	});
}

// Получаем пезультат парсера капчи
function ResultParseCapcha(taskId, iinBin){
	$.ajax({
		type : "POST",
		url: '/query/result-parsecapcha',			
		data : {taskId: taskId},
		success: function(data){
			var obj = jQuery.parseJSON(data);
			if(obj.errorId == 0 && obj.status == 'ready'){
				var capchaText = obj.solution['text'];
				getData(capchaText, iinBin);
			}else{
				ResultParseCapcha(taskId, iinBin);
			}
		}
	});
}

// Стартуем команду на парсинг
function startFunction(){
	$('.error_iinBin').attr('style', 'display: none;')
	$('#result').attr('style', 'display: none;');
	
	iinBin = $('#iinBin').val();
	length = 12;
	error = 0;
	
	if(!iinBin || iinBin == ''){
		var text = 'Поле «ИИН/БИН» обязательно для заполнения.<br />Обнаружены ошибки ввода символьного кода.';
		$('.error_iinBin').attr('style', 'color: red; display: block;').html(text);
		error = 1;
	}else{
		if (!iinBin.match(new RegExp('^\\d{' + length + '}$'))) {
			var text = 'Поле «ИИН/БИН» должно содержать в себе 12 цифр.<br />Обнаружены ошибки ввода символьного кода';
			$('.error_iinBin').attr('style', 'color: red; display: block;').html(text);
			error = 1;
		}else{
			if (!validateUinCheckSumValue(iinBin)) {
				var text = 'Введенное значение не соответствует ИИН/БИН.<br />Обнаружены ошибки ввода символьного кода';
				$('.error_iinBin').attr('style', 'color: red; display: block;').html(text);
				error = 1;
			}
		}
	}
	
	if (error == 0) {
		ParseCapcha(iinBin);
	}
}
	
// Отправляем данные для получения Выходных данных
function getData(capchaText, iinBin){
	// Скрипт по передачи и получения результата
	var url = "https://cors-anywhere.herokuapp.com/http://kgd.gov.kz/apps/services/culs-taxarrear-search-web/rest/search";
	var data = {
		'iinBin': iinBin,
		'captcha-user-value': capchaText,
		'captcha-id': captchaUid
	};
	
	$.ajax({
		crossOrigin: 'https://kgd.gov.kz',
		headers: {
			'Accept': 'application/json',
			'Content-Type': 'application/json'
		},
		type : "POST",
		url: url,			
		dataType: 'json',
		data : JSON.stringify(data),
		success: function(data){
			console.log(data);
			
			// var obj = jQuery.parseJSON(data);
			
			
			if(data.captchaError == 'captcha-error'){
				alert('ошибка');
				// ParseCapcha(iinBin);
			}else{
				// console.log(obj);			
				$('#table0 #table1_name').text(data.nameRu);
				$('#table0 #table1_iinBin').text(data.iinBin);
				$('#table0 #table1_totalArrear').text(data.totalArrear);
				
				$('#table1 #table2_totalTaxArrear').text(data.totalTaxArrear);
				$('#table1 #table2_pensionContributionArrear').text(data.pensionContributionArrear);
				$('#table1 #table2_socialHealthInsuranceArrear').text(data.socialHealthInsuranceArrear);
				$('#table1 #table2_socialContributionArrear').text(data.socialContributionArrear);
				
				$('#result').attr('style', 'display: block !important;');
			}
		
		}
	});

	reloadCaptcha();
}

</script>