<?php
namespace frontend\controllers;

use Yii;
use yii\web\Controller;

// v���������� �������
use frontend\components\BackendFunctions;

class QueryController extends Controller {
	
	public $layout = 'empty';
	

	public function actionParseCapcha() {
		if (Yii::$app->request->post()) {
			$captchaImg = Yii::$app->request->post('captchaImg');
			$body = base64_encode(file_get_contents($captchaImg));
			
			$url_Capcha = Yii::$app->params['urlCapcha'];
			
			$values = array(
				'clientKey' => 'ba774133fd19e03a02175d9f76fd9bc6',
				'task' => array(
					'type' => 'ImageToTextTask',
					'body' => $body,
					'phrase' => false,
					'case' => false,
					'numeric' => false,
					'math' => 0,
					'minLength' => 0,
					'maxLength' => 0
				)
			);
			
			$result = BackendFunctions::PostFunctions($url_Capcha, $values);	
			return $result;
		}
	}
	
	public function actionResultParsecapcha() {
		if (Yii::$app->request->post()) {
			$taskId = Yii::$app->request->post('taskId');
			
			
			$url_Capcha_Result = Yii::$app->params['urlCapchaResult'];
			
			$values = array(
				'clientKey' => 'ba774133fd19e03a02175d9f76fd9bc6',
				'taskId' => $taskId
			);
			sleep(5);
			$result = BackendFunctions::PostFunctions($url_Capcha_Result, $values);	
			return $result;
		}
	}
}