<?php
	
	namespace frontend\components;
	use Yii;
	
	
	class  BackendFunctions{
		
		public static function GetFunctions($url){
			$cookie = dirname(__DIR__)."/cookie.txt";
			$uagent = "Opera/9.80 (Windows NT 6.1; WOW64) Presto/2.12.388 Version/12.14";	 
			$uagent = "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/54.0.2840.87 Safari/537.36";
			$ch = curl_init( $url );	 
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1); 
			curl_setopt($ch, CURLOPT_USERAGENT, $uagent);	  
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE); 
			curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
			curl_setopt($ch, CURLOPT_COOKIEFILE,$cookie);
			curl_setopt($ch, CURLOPT_COOKIEJAR,$cookie);  
			$result = curl_exec( $ch );
			curl_close($ch);
			
			return $result;			
		}
		
		
		public static function PostFunctions($url, $values){
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL, $url);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($ch, CURLOPT_POST, true);
			curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($values));
			curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
			$result = curl_exec($ch);
			curl_close($ch);

			return $result;		
		}	
	}